package com.thepilab.squack;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Handler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joekratzat on 11/4/15.
 */
@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class ToastExample {

    private Context context;
    private static ToastExample instance;
    private BluetoothAdapter mBluetoothAdapter;
    private final static int REQUEST_ENABLE_BT = 1;
    private static final long SCAN_PERIOD = 10000;
    private boolean mScanning;
    private Handler mHandler;
    private ArrayList<BluetoothDevice> mLeDevices;
    private BluetoothLeScanner mLEScanner;
    private ScanSettings settings;
    private List<ScanFilter> filters;
    private BluetoothGatt mGatt;

    public ToastExample() {
        this.instance = this;
    }

    public static ToastExample instance() {
        if(instance == null) {
            instance = new ToastExample();
        }
        return instance;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void showMessage(String message) {
//        Toast.makeText(this.context, message, Toast.LENGTH_SHORT).show();
        Log.v("TOAST", "=--------------------> Scanning");
        mScanning = false;
        mHandler = new Handler();
        mLeDevices = new ArrayList<BluetoothDevice>();
        if (!context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this.context, "BLE not supported", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this.context, "BLE IS READY", Toast.LENGTH_SHORT).show();
        }

        final BluetoothManager bluetoothManager =
                (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
//            TODO: Need to ask user to turn on
//            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        if (Build.VERSION.SDK_INT >= 21) {
            mLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
            if (Build.VERSION.SDK_INT >= 23) {
                Toast.makeText(context, "SDK >= 23", Toast.LENGTH_SHORT).show();
                settings = new ScanSettings.Builder()
//                    TODO: API 23 or above only
                        .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES | ScanSettings.SCAN_MODE_LOW_POWER)
                        .setScanMode(ScanSettings.SCAN_MODE_LOW_POWER)
                        .build();
            }else{
                settings = new ScanSettings.Builder()
                        .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                        .build();
            }
            filters = new ArrayList<ScanFilter>();
        }
        scanLeDevice(true);
    }

    // Device scan callback.
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {

                @Override
                public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
                    Toast.makeText(context, device.getName(), Toast.LENGTH_SHORT).show();
                    if(!mLeDevices.contains(device)) {
                        mLeDevices.add(device);
                    }
                }
            };

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    StringBuilder builder = new StringBuilder();
                    builder.append("FOUND DEVICES ");
                    builder.append(mLeDevices.size());
                    Toast.makeText(context, builder.toString(), Toast.LENGTH_SHORT).show();
                    mScanning = false;
                    if (Build.VERSION.SDK_INT < 21) {
                        mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    } else {
                        mLEScanner.stopScan(mScanCallback);

                    }
                }
            }, SCAN_PERIOD);

            mScanning = true;
            if (Build.VERSION.SDK_INT < 21) {
                Toast.makeText(context, "SDK < 21", Toast.LENGTH_SHORT).show();
                mBluetoothAdapter.startLeScan(mLeScanCallback);
            } else {
                Toast.makeText(context, "SDK >= 21", Toast.LENGTH_SHORT).show();
                mLEScanner.startScan(filters, settings, mScanCallback);
            }
        } else {
            mScanning = false;
            if (Build.VERSION.SDK_INT < 21) {
                mBluetoothAdapter.stopLeScan(mLeScanCallback);
            } else {
                mLEScanner.stopScan(mScanCallback);
            }
        }
    }

    private ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
//            Toast.makeText(context, "onScanResult", Toast.LENGTH_SHORT).show();
            Log.i("callbackType", String.valueOf(callbackType));
            Log.i("result", result.toString());
            BluetoothDevice btDevice = result.getDevice();
//            Toast.makeText(context, result.getDevice().getName(), Toast.LENGTH_SHORT).show();
            if(!mLeDevices.contains(result.getDevice())) {
                mLeDevices.add(result.getDevice());
            }
//            connectToDevice(btDevice);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            Toast.makeText(context, "onBatchScanResults", Toast.LENGTH_SHORT).show();
            for (ScanResult sr : results) {
                Log.i("ScanResult - Results", sr.toString());
                Toast.makeText(context, sr.toString(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            Toast.makeText(context, "onScanFailed", Toast.LENGTH_SHORT).show();
            Log.e("Scan Failed", "Error Code: " + errorCode);
            Toast.makeText(context, "Scan Failed Error Code: " + errorCode, Toast.LENGTH_SHORT).show();
        }
    };

}
