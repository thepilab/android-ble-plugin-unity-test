﻿using UnityEngine;
using System.Collections;

public class ToastExample : MonoBehaviour {
	
	private AndroidJavaObject toastExample = null;
	private AndroidJavaObject activityContext = null;
	
	void Start() {
		Debug.Log ("--------------- START");
		Debug.Log ("--------------- ToastExample 1");
		if(toastExample == null) {
			Debug.Log ("--------------- ToastExample 2");
			using(AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer")) {
				Debug.Log ("--------------- ToastExample 3");
				activityContext = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
				Debug.Log ("--------------- ToastExample 3a");
			}
			Debug.Log ("--------------- ToastExample 3b");
			using(AndroidJavaClass pluginClass = new AndroidJavaClass("com.thepilab.squack.ToastExample")) {
				Debug.Log ("--------------- ToastExample 4");
				if(pluginClass != null) {
					Debug.Log ("--------------- ToastExample 5");
					toastExample = pluginClass.CallStatic<AndroidJavaObject>("instance");
					toastExample.Call("setContext", activityContext);
					activityContext.Call("runOnUiThread", new AndroidJavaRunnable(() => {
						toastExample.Call("showMessage", "This is a Toast message");
					}));
					Debug.Log ("--------------- ToastExample 6");
				}
			}
			Debug.Log ("--------------- ToastExample something");
		}
		Debug.Log ("--------------- ToastExample END");
	}
}